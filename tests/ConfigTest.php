<?php

use PHPUnit\Framework\TestCase;
use System\Config;

final class ConfigTest extends TestCase
{
    public function testArrayConfigSame(): void
    {
        $conf = Config::getInstance()->getConfig(Config::CONFIG_FILE);
        $this->assertSame(
            ['db' => ['type' => 'mysql', 'user' => ['login' => 'vasya', 'password' => 'qwerty']]],
            $conf
        );
    }

    public function testArrayConfigNotSame(): void
    {
        $conf = Config::getInstance()->getConfig(Config::CONFIG_FILE);
        $this->assertNotSame(
            ['db' => ['typed' => 'mysql', 'user' => ['login' => 'vasya', 'password' => 'qwerty']]],
            $conf
        );
    }
}

