<?php

namespace System;

class Config
{
    /**
     * config file's name
     */
    const CONFIG_FILE = 'config';

    /**
     * delimiter for key and value
     */
    const DELIMITER = '=';
    const DELIMITER_PATH = '.';

    private static $_instance = null;

    private function __construct()
    {
    }

    /**
     * @return Config|null
     */
    public static function getInstance()
    {
        return self::$_instance ? self::$_instance : (self::$_instance = new self());
    }

    /**
     * @param string $file
     * @return array
     */
    public function getConfig(string $file)
    {
        $rows = $this->_loadConfig($file);
        $result = [];
        foreach ($rows as $row) {
            $keyValue = explode(self::DELIMITER, $row, 2);
            if (count($keyValue) === 2) {
                $this->_toArray($result, trim($keyValue[0]), trim($keyValue[1]));
            }
        }

        return $result;
    }

    private function _loadConfig(string $file)
    {
        $data = file_get_contents($file);
        $rows = explode("\n", $data);

        return $rows;
    }

    private function _toArray(&$array, $path, $value)
    {
        if (is_null($path)) {
            $array = $value;
            return;
        }

        $keys = is_array($path) ? $path : explode(self::DELIMITER_PATH, $path);
        $last = array_pop($keys);

        foreach ($keys as $key) {
            if (!isset($array[$key])) {
                $array[$key] = [];
            }
            if (!is_array($array[$key])) {
                $array[$key] = [$array[$key]];
            }
            $array = &$array[$key];
        }

        $array[$last] = $value;
    }
}